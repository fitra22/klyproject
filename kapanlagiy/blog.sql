-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 12 Jan 2020 pada 09.55
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blog`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `guru`
--

CREATE TABLE `guru` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `telpon` varchar(15) NOT NULL,
  `alamat` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `guru`
--

INSERT INTO `guru` (`id`, `nama`, `telpon`, `alamat`, `created_at`, `updated_at`) VALUES
(1, 'Tono', '0898709008', 'malang', '2020-01-11 14:26:16', '0000-00-00 00:00:00'),
(2, 'jiko', '0956576325', 'jember', '2020-01-11 14:26:16', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel`
--

CREATE TABLE `mapel` (
  `id` int(11) NOT NULL,
  `kode` varchar(191) NOT NULL,
  `nama` varchar(191) NOT NULL,
  `semester` varchar(45) NOT NULL,
  `guru_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mapel`
--

INSERT INTO `mapel` (`id`, `kode`, `nama`, `semester`, `guru_id`, `created_at`, `updated_at`) VALUES
(1, 'M-01', 'Matematika Dasar', 'Ganjil', 1, '2020-01-09 13:55:33', '0000-00-00 00:00:00'),
(2, 'B-01', 'Bahasa Indonesia', 'Ganjil', 2, '2020-01-09 13:55:33', '0000-00-00 00:00:00'),
(3, 'A-01', 'Agama', 'Ganjil', 2, '2020-01-11 14:27:36', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Struktur dari tabel `mapel_siswa`
--

CREATE TABLE `mapel_siswa` (
  `id` int(11) NOT NULL,
  `siswa_id` int(11) NOT NULL,
  `mapel_id` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `mapel_siswa`
--

INSERT INTO `mapel_siswa` (`id`, `siswa_id`, `mapel_id`, `nilai`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 99, '2020-01-10 00:27:45', '2020-01-10 07:27:45'),
(4, 4, 1, 88, '2020-01-10 00:41:36', '2020-01-10 07:41:36'),
(5, 4, 2, 88, '2020-01-10 00:41:54', '2020-01-10 07:41:54'),
(7, 1, 2, 99, '2020-01-11 02:10:01', '2020-01-11 09:10:01'),
(11, 9, 1, 66, '2020-01-11 07:34:14', '2020-01-11 15:20:25'),
(12, 9, 3, 88, '2020-01-11 08:14:10', '2020-01-11 15:14:10'),
(13, 9, 2, 77, '2020-01-11 08:14:20', '2020-01-11 15:14:20'),
(14, 12, 1, 88, '2020-01-11 08:20:47', '2020-01-11 15:20:47'),
(15, 12, 2, 98, '2020-01-11 08:20:55', '2020-01-11 15:20:55'),
(16, 12, 3, 98, '2020-01-11 08:21:07', '2020-01-11 15:21:07'),
(17, 15, 1, 88, '2020-01-11 08:25:24', '2020-01-11 15:25:24'),
(18, 15, 2, 77, '2020-01-11 08:25:33', '2020-01-11 15:25:33'),
(19, 15, 3, 76, '2020-01-11 08:25:59', '2020-01-11 15:25:59'),
(20, 17, 1, 66, '2020-01-11 08:26:15', '2020-01-11 15:26:15'),
(21, 17, 2, 65, '2020-01-11 08:26:24', '2020-01-11 15:26:24'),
(22, 17, 3, 98, '2020-01-11 08:26:36', '2020-01-11 15:26:36'),
(23, 18, 1, 56, '2020-01-11 08:26:51', '2020-01-11 15:26:51'),
(24, 18, 2, 65, '2020-01-11 08:26:59', '2020-01-11 15:26:59'),
(25, 18, 3, 76, '2020-01-11 08:27:10', '2020-01-11 15:27:10'),
(26, 19, 1, 89, '2020-01-11 08:27:31', '2020-01-11 15:27:31'),
(27, 19, 2, 97, '2020-01-11 08:27:44', '2020-01-11 15:27:44'),
(28, 19, 3, 96, '2020-01-11 08:27:54', '2020-01-11 15:27:54'),
(29, 20, 3, 88, '2020-01-11 22:45:58', '2020-01-12 05:45:58'),
(30, 20, 1, 67, '2020-01-11 22:46:08', '2020-01-12 05:46:08'),
(31, 20, 2, 66, '2020-01-11 22:46:17', '2020-01-12 05:46:17');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_01_05_090559_create_siswa_table', 2);

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `siswa`
--

CREATE TABLE `siswa` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `nama_depan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nama_belakang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `jenis_kelamin` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_telp` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `date_of_birth` date NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `siswa`
--

INSERT INTO `siswa` (`id`, `user_id`, `nama_depan`, `nama_belakang`, `jenis_kelamin`, `no_telp`, `date_of_birth`, `avatar`, `created_at`, `updated_at`) VALUES
(9, 8, 'Roni', 'Setiawanto', 'L', '0877878698', '1994-03-15', NULL, '2020-01-09 00:34:11', '2020-01-11 20:14:38'),
(12, 11, 'Agill', 'Falah', 'P', '09838392282', '2000-01-11', 'user2.png', '2020-01-11 02:47:41', '2020-01-11 22:44:32'),
(15, 14, 'joko', 'kolo', 'L', '34642642677', '1998-03-31', NULL, '2020-01-11 02:59:10', '2020-01-11 20:26:24'),
(17, 16, 'Dika', 'Duki', 'P', '6356535787', '1993-03-31', NULL, '2020-01-11 08:19:23', '2020-01-11 20:26:42'),
(18, 17, 'Hani', 'Hana', 'P', '4375737657', '1991-04-05', NULL, '2020-01-11 08:19:59', '2020-01-11 20:26:59'),
(19, 1, 'Fitra', 'Firdaus', 'L', '32536466366', '1999-01-15', 'user5.png', NULL, '2020-01-11 22:45:27'),
(20, 18, 'Ucup', 'Sahi', 'L', '656377678585', '1989-03-04', NULL, '2020-01-11 18:59:36', '2020-01-11 20:27:48');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `role` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `role`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Fitra', 'Fitra@gmail.com', NULL, '$2y$10$BPdXEZlpjbr2fwMF75Dkh.h4Km2DTCZZL8Xl0tYHHCioJ2xSFwauu', 'kGKlzTVAkbNNKrDFFiZ8R6cbKWnOxAu7fYi5YGHwa4KATWvXwhx8pQSitiSR', '2020-01-06 03:10:47', '2020-01-06 03:10:47'),
(8, 'siswa', 'Roni', 'roni@gmail.com', NULL, '$2y$10$Z35YrjdAIXATfzQKFaLaG.YY6bcBEml2zwHeoz.hzLn61ve6RjEUe', 'PMDtPm8juuwXm8Rjr2Mdf8nCNWnzVNc9HCFnczRc8nlH6z3iG4AiCry7krcr', '2020-01-09 00:34:11', '2020-01-09 00:34:11'),
(11, 'siswa', 'Agill', 'agil@gmail.com', NULL, '$2y$10$nyxGXLe6uQme8kDw0F3ktumkywmCbIoBfrcOSZxJcW01IAjwo9ene', 'KwVbzRPPTJTkkvh2lfFIr49TlOTuDEGkbuarkm8ZMd9FXvX1oq3gJ3zrUHce', '2020-01-11 02:47:41', '2020-01-11 02:47:41'),
(14, 'admin', 'joko', 'joko@gmail.com', NULL, '$2y$10$LvaqN4kRFIYlfwC3ncRaV.iVssNF4S4UJ04jwVrbmY5mmnJDTdHp6', 'cdtAk4edL2hVYclTOKxT4oFXGYRRpfUIKaciRu3w5tyMJRysuMLVBDQbvPye', '2020-01-11 02:59:10', '2020-01-11 02:59:10'),
(15, 'admin', 'aewga', 'ada@gmail.com', NULL, '$2y$10$CJPnwxbnBFh5rbc2UNKgwekKivFubICPGjOjwQDHvePwAxBmL/8rK', 'R8zLDhVszv6UblMRH24CBzo48gwDNHiXXUq9Qbj2w57AxZQtnl2DqqEejhbn', '2020-01-11 03:31:36', '2020-01-11 03:31:36'),
(16, 'siswa', 'Dika', 'dika@gmail.cm', NULL, '$2y$10$l20CxBBopn9lkOlA7RC4mOZVJbcPQdliPAyvzviJLmSWd4453IJia', 'vf3it01Vvi6KyBlm8vNPcBd8kaqjVEtoqNXE5czjCcuiMQ0wx6ls3gK2Hmk2', '2020-01-11 08:19:23', '2020-01-11 08:19:23'),
(17, 'siswa', 'Hani', 'hani@gmail.com', NULL, '$2y$10$R5FuPuBzCf6bXP4Pd/HzuOtNHCIG6Zw5VeWtyJSlZBIXZgI6nXAD2', 'T8i3ptjdptffNRI7ep7YKoCdsJflNuwEN8M7OCyjDlzygT8Ch77xnhjlBk2N', '2020-01-11 08:19:59', '2020-01-11 08:19:59'),
(18, 'siswa', 'Ucup', 'ucup@gmail.com', NULL, '$2y$10$lM/47D7r4l1CqBO2Zrh8WeKwo1GU7HXfUvCyN3GS0Tl4T9zwZZ2Ri', 'ZXL6a0UbI8rypzHLqwSvFrduYmd7CPCGSn13rRkWvCCwR3pyf4ehGWOn8opz', '2020-01-11 18:59:36', '2020-01-11 18:59:36');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `guru`
--
ALTER TABLE `guru`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mapel`
--
ALTER TABLE `mapel`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `mapel_siswa`
--
ALTER TABLE `mapel_siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indeks untuk tabel `siswa`
--
ALTER TABLE `siswa`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `guru`
--
ALTER TABLE `guru`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `mapel`
--
ALTER TABLE `mapel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `mapel_siswa`
--
ALTER TABLE `mapel_siswa`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT untuk tabel `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `siswa`
--
ALTER TABLE `siswa`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
