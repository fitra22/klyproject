@extends('layout.master')

@section('content')
<div class="main">
	<div class="main-content">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-12">
					<div class="panel">
								<div class="panel-heading">
									<h3 class="panel-title">Data Siswa</h3>
									<div class="right">
										<button type="button" class="btn" data-toggle="modal" data-target="#exampleModalCenter"><i class="lnr lnr-plus-circle"></i></button>
									</div>
								</div>
								<div class="panel-body">
									<table class="table table-hover">
										<thead>
											<tr>
												<th>Nama Depan</th>
												<th>Nama Belakang</th>
												<th>Jenis Kelamin</th>
												<th>No Telepon</th>
												<th>Date of Birth</th>
												<th>Total Rata2</th>
												<th>Aksi</th>
											</tr>
										</thead>
										<tbody>
											@foreach($data_siswa as $siswa)
												<tr>
													<td><a href="/siswa/{{$siswa->id}}/profile">{{$siswa->nama_depan}}</a></td>
													<td><a href="/siswa/{{$siswa->id}}/profile">{{$siswa->nama_belakang}}</a></td>
													<td>{{$siswa->jenis_kelamin}}</td>
													<td>{{$siswa->no_telp}}</td>
													<td>{{$siswa->date_of_birth}}</td>
													<td>{{$siswa->rataRataNilai()}}</td>
													<td>
														<a href="/siswa/{{$siswa->id}}/edit" class="btn btn-warning btn-sm">Edit</a>
														<a href="/siswa/{{$siswa->id}}/delete" class="btn btn-danger btn-sm" onclick="return confirm('Are you sure ?')">Delete</a>
													</td>

												</tr>
											@endforeach
										</tbody>
									</table>
								</div>
							</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
			  <div class="modal-dialog modal-dialog" role="document">
			    <div class="modal-content">
			      <div class="modal-header">
			        <h5 class="modal-title" id="exampleModalCenterTitle">Modal title</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button>
			      </div>
			      <div class="modal-body">
			        <form action="/siswa/create" method="POST" enctype="multipart/form-data">
			        	{{csrf_field()}}
					  <div class="form-group{{$errors->has('nama_depan') ? ' has-error' : ''}}">
					    <label for="exampleInputEmail1">Nama Depan</label>
					    <input name="nama_depan" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Nama Depan" value="{{old('nama_depan')}}">
						@if($errors->has('nama_depan'))
							<span class="help-block">{{$errors->first('nama_depan')}}</span>
						@endif
					  </div>
					  <div class="form-group{{$errors->has('nama_belakang') ? ' has-error' : ''}}">
					    <label for="exampleInputPassword1">Nama Belakang</label>
					    <input name="nama_belakang" type="text" class="form-control" id="exampleInputPassword1" placeholder="Nama Belakang" value="{{old('nama_belakang')}}">
						@if($errors->has('nama_belakang'))
							<span class="help-block">{{$errors->first('nama_belakang')}}</span>
						@endif
					  </div>
					  
					  <div class="form-group{{$errors->has('email') ? ' has-error' : ''}}" >
					    <label for="exampleInputPassword1">Email</label>
					    <input name="email" type="email" class="form-control" id="exampleInputPassword1" placeholder="Email"value="{{old('email')}}">
						@if($errors->has('email'))
							<span class="help-block">{{$errors->first('email')}}</span>
						@endif
					  </div>

					  <div class="form-group{{$errors->has('jenis_kelamin') ? ' has-error' : ''}}">
					    <label for="exampleFormControlSelect1">Pilih Jenis Kelamin</label>
					    <select name="jenis_kelamin" class="form-control" id="exampleFormControlSelect1">
					      <option value="L"{{(old('jenis_kelamin')  ==  'L') ? ' selected ' : ''}}>Lelaki</option>
					      <option value="P"{{(old('jenis_kelamin')  ==  'L') ? ' selected ' : ''}}>Perempuan</option>
					    </select>
						@if($errors->has('jenis_kelamin'))
							<span class="help-block">{{$errors->first('jenis_kelamin')}}</span>
						@endif
					  </div>

					  <div class="form-group{{$errors->has('role') ? ' has-error' : ''}}">
					    <label for="exampleFormControlSelect1">Pilih Role</label>
					    <select name="role" class="form-control" id="exampleFormControlSelect1">
					      <option value="admin"{{(old('role')) ? ' selected ' : ''}}>Admin</option>
					      <option value="siswa"{{(old('role')) ? ' selected ' : ''}}>Siswa</option>
					    </select>
						@if($errors->has('role'))
							<span class="help-block">{{$errors->first('role')}}</span>
						@endif
					  </div>

					  <!-- <div class="form-group{{$errors->has('role') ? ' has-error' : ''}}">
					    <label for="exampleInputEmail1">Role</label>
					    <input name="role" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="role" value="{{old('role')}}">
						@if($errors->has('role'))
							<span class="help-block">{{$errors->first('role')}}</span>
						@endif
					  </div> -->

					  <div class="form-group{{$errors->has('no_telp') ? ' has-error' : ''}}">
					    <label for="exampleInputEmail1">No Telepon</label>
					    <input name="no_telp" type="text" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="No Telepon" value="{{old('no_telp')}}">
						@if($errors->has('no_telp'))
							<span class="help-block">{{$errors->first('no_telp')}}</span>
						@endif
					  </div>

					  <div class="form-group{{$errors->has('date_of_birth') ? ' has-error' : ''}}">
						<label for="example-date-input" class="col-2 col-form-label">Date of Birth</label>
						<div class="col-10">
							<input name="date_of_birth" type="date" class="form-control" id="example-date-input" placeholder="Date of Birth" value="{{old('date_of_birth')}}">
							@if($errors->has('date_of_birth'))
							<span class="help-block">{{$errors->first('date_of_birth')}}</span>
							@endif
						</div>

					</div>

					   <!-- <div class="form-group">
					    <label for="exampleFormControlTextarea1">Alamat</label>
					    <textarea name="alamat" class="form-control" id="exampleFormControlTextarea1" rows="3">{{old('alamat')}}</textarea>
					  </div> -->

					  <div class="form-group{{$errors->has('avatar') ? ' has-error' : ''}}">
					  	<label for="exampleFormControlTextarea1">Avatar</label>
						<input type="file" name="avatar" class="form-control">
						@if($errors->has('avatar'))
							<span class="help-block">{{$errors->first('avatar')}}</span>
						@endif
					  </div>

					
					
					
		
			      </div>
			      <div class="modal-footer">
			        <button type="reset" value="reset" class="btn btn-secondary" >Reset</button>
			        <button type="submit" class="btn btn-primary">Submit</button>
			        </form>
			      </div>
			    </div>
			  </div>
@stop

			
