<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SiswaController extends Controller
{
    public function index(Request $request)
    {
    	if($request->has('cari')){
    		$data_siswa = \App\Siswa::where('nama_depan','LIKE', '%'.$request->cari.'%')->get();
    	}else{
    		$data_siswa = \App\Siswa::all();
    	}
    	
    	return view('siswa.index',['data_siswa' => $data_siswa]);
    }

    public function create(Request $request)
    {
		$this->validate($request,[

			'nama_depan' => 'required',
			'nama_belakang' => 'required',
			'email' => 'required|email|unique:users',
			'jenis_kelamin' => 'required',
			'no_telp' => 'required|numeric',
			'date_of_birth' => 'required',
			'avatar' => 'mimes:jpg,png'

		]);

		// insert ke table users
		$user = new \App\User;
		$user->role = $request ->role;
		$user->name = $request->nama_depan;
		$user->email = $request->email;
		$user->password = bcrypt('rahasia');
		$user->remember_token = \Str::random(60);
		$user->save();

		//insert ke table Siswa
		$request->request->add(['user_id' => $user->id]);
		$siswa = \App\Siswa::create($request->all());
		if($request->hasFile('avatar')){
			$request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
			$siswa->avatar = $request->file('avatar')->getClientOriginalName();
			$siswa->save();
		}
    	return redirect('/siswa')->with('sukses','Terima kasih telah mengisi form');
    }

    public function edit($id)
    {
    	$siswa = \App\Siswa::find($id);
    	return view('siswa/edit',['siswa' => $siswa]);
    }

    public function update(Request $request, $id)
    {
		// dd($request->all());
    	$siswa = \App\Siswa::find($id);
		$siswa->update($request->all());
		if($request->hasFile('avatar')){
			$request->file('avatar')->move('images/',$request->file('avatar')->getClientOriginalName());
			$siswa->avatar = $request->file('avatar')->getClientOriginalName();
			$siswa->save();
		}
    	return redirect('/siswa')->with('sukses','Data berhasi diupdate !!');
    }

    public function delete($id)
    {
    	$siswa = \App\Siswa::find($id);
    	$siswa->delete();
    	return redirect('/siswa')->with('sukses','Data berhasi dihapus !!');
	}
	
	public function profile($id)
	{
		$siswa = \App\Siswa::find($id);
		$matapelajaran = \App\Mapel::all();
		return view('/siswa/profile',['siswa' => $siswa,'matapelajaran' => $matapelajaran]);
	}

	public function addnilai(Request $request,$idsiswa)
	{
		// dd($request->all());
		$siswa = \App\Siswa::find($idsiswa);
		if($siswa->mapel()->where('mapel_id',$request->mapel)->exists()){
			return redirect('siswa/'.$idsiswa.'/profile')->with('error','Data sudah ada');

		}
		$siswa->mapel()->attach($request->mapel,['nilai' => $request->nilai]);

		// return redirect('siswa/'.$idsiswa.'/profile')->with('sukses','Data Nilai berhasi diinput !!');
		return redirect('siswa/'.$idsiswa.'/profile')->with('sukses','Data berhasil input');

	}

	public function deletenilai($idsiswa,$idmapel)
	{
		$siswa = \App\Siswa::find($idsiswa);
		$siswa->mapel()->detach($idmapel);
		return redirect()->back()->with('sukses','Data nilai berhasil dihapus');
	}
	
}
